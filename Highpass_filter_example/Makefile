###############################################################################
##
## Copyright(c) 2003-2015 Fraunhofer Gesellschaft. All rights reserved.
##              Institut Integrierte Schaltungen - EAS Dresden
##
## Copyright(c) 2015 COSEDA Technologies GmbH.
##              All rights reserved.
##
###############################################################################

###############################################################################
#
#          NORMALLY IN THIS MAKEFILE NO CHANGES ARE NEEDED!
#
###############################################################################

###############################################################################
# Project configuration - include _config_data
###############################################################################

# customize _config_data filename
CONFIG_DATA ?= _config_data

# customize _config_data_cc filename
CONFIG_DATA_CC ?= _config_data_cc

# customize _config_data_hdl filename
CONFIG_DATA_HDL ?= _config_data_hdl

###############################################################################
# include config_data
###############################################################################
include $(CONFIG_DATA)

###############################################################################
# include config_data_cc for RCP/HiL targets
###############################################################################
-include $(CONFIG_DATA_CC)

###############################################################################
# include config_data_hdl for VHDL/Verilog compiler
###############################################################################
-include $(CONFIG_DATA_HDL)

###############################################################################
# customize -I, -L, -l sections before default COSIDE library section
# usage: Use explicitly "+=" instead of "="!!
#        "SYSTEMCINCDIR +=" to add include search paths
#        "SYSTEMCLIBDIR +=" to add library search paths
#        "SYSTEMCLIBS   +=" to add library
#
# to invoke user defined Makefiles create Makefile_local_pre.mk and add e.g.:
#        USER_MAKEFILE = UserMakefile.mk
#        call_rule_pre_all:
#            @echo "" 
#            @echo "Call \"user_rule_pre_all\" within \"$(USER_MAKEFILE)\""
#            $(MAKE) -f $(USER_MAKEFILE) user_rule_pre_all
###############################################################################
-include Makefile_local_pre.mk

###############################################################################
# include COSIDE libraries -I, -L, -l sections, do not change
###############################################################################
include $(MAKEFLOW_HOME)/Makefile_global_inc_libs.mk

###############################################################################
# customize -I, -L, -l sections after default COSIDE library section
# see Makefile_local_pre.mk section comment above
###############################################################################
-include Makefile_local_pos.mk

###############################################################################
# define what to do when make is called w/o target regardless of the target
# definition order in this makefile
###############################################################################
.DEFAULT_GOAL := all
